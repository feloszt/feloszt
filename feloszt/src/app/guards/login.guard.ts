import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { NavBarService } from '../nav-bar/nav-bar.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(private navBarService: NavBarService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      console.log('LoginGuard Activated');
      console.log(this.navBarService.checkIfPlayerIsLoggedIn());
      return this.navBarService.checkIfPlayerIsLoggedIn();
  }
}
