export class RoleModel {
  roles: Array<Role>;
  constructor() {
    this.roles = new Array<Role>();
   }
}

export class Role {
  Id?: number;
  Name: string;
  Description: string;
  RoleType: string;
  IsReadAccess: boolean;
  IsWriteAccess: boolean;
  IsEditAccess: boolean;
  IsDeleteAccess: boolean;
  HasCategoryCreatePower: boolean;
  HasCategoryDeletePower: boolean;
  HasCategoryMovePower: boolean;
  HasCategoryModifyPower: boolean;
  HasArchivalPower: boolean;
  CanPromoteOthers: boolean;
}
