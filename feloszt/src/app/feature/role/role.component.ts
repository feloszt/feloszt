import { Component, OnInit } from '@angular/core';
import { RoleModel } from './role-model';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {

  roleModel: RoleModel;
  constructor() { }

  ngOnInit() {
    this.roleModel = new RoleModel();
  }

}
