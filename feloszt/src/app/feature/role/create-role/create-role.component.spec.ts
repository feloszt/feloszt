import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreateRoleComponent } from './create-role.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

describe('CreateRoleComponent', () => {
  let component: CreateRoleComponent;
  let fixture: ComponentFixture<CreateRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateRoleComponent ],
      providers: [{provide: MatDialogRef, useValue: {}},
      {provide: MAT_DIALOG_DATA, useValue: {}}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
