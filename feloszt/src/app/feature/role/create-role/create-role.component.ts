import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Role } from '../role-model';
@Component({
  selector: 'app-create-role',
  templateUrl: './create-role.component.html',
  styleUrls: ['./create-role.component.css'],
})
export class CreateRoleComponent implements OnInit {

  form: FormGroup;
  formBuilder: FormBuilder = new FormBuilder();

  constructor(public dialogRef: MatDialogRef<CreateRoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.form = this.generateForm();
  }
  saveRole(): void {
    this.dialogRef.close(this.generateReturnRoleObject());
  }
  requiresModPerms(): boolean {
    return !(this.form.controls['roleType'].value === 'Basic');
  }
  closeModal(): void {
    this.dialogRef.close(null);
  }
  private generateReturnRoleObject(): Role {
    const desc: string = (this.form.controls['roleDetails'].get('description').value) ?
    `${this.form.controls['roleDetails'].get('description').value}` : 'no description provided';
    return Object.assign(new Role(),{
      Name: `${this.form.controls['roleDetails'].get('name').value}`,
      Description: `${desc}`,
      RoleType: `${this.form.controls['roleType'].value}`,
      IsReadAccess: this.form.controls['rolePermissions'].get('isReadAccess').value,
      IsWriteAccess: this.form.controls['rolePermissions'].get('isWriteAccess').value,
      IsEditAccess: this.form.controls['rolePermissions'].get('isEditAccess').value,
      IsDeleteAccess: this.form.controls['rolePermissions'].get('isDeleteAccess').value,
      HasCategoryCreatePower: false,
      HasCategoryDeletePower: false,
      HasCategoryMovePower: false,
      HasCategoryModifyPower: false,
      HasArchivalPower: false,
      CanPromoteOthers: false
    });
  }
  private generateForm(): FormGroup {
    return this.formBuilder.group({
      roleDetails: this.formBuilder.group({
        name: ['', [Validators.minLength(3), Validators.maxLength(50), Validators.required]],
        description: ['', Validators.maxLength(255)]
      }),
      roleType: ['Primary'],
      rolePermissions: this.formBuilder.group({
        isReadAccess: [false],
        isWriteAccess: [false],
        isEditAccess: [false],
        isDeleteAccess: [false]
      })
    });
  }
}
