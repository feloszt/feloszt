import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  isClosed: boolean = false;
  titleString: string;
  urlString: string;
  hierarchy: number;
  boardList: Array<any> = [];

  @Input('title') set title (value: string) {
    if (value) {
      this.titleString = value;
    }
  }

  @Input('url') set url (value: string) {
    if (value) {
      this.urlString = value;
    }
  }
  constructor(private router: Router) { }

  ngOnInit() {
    this.hierarchy = 1;
    this.boardList=[{title: 'Board #1', url: 'board1', mods: 'Celestials', summary: 'This is a test'}, 
    {title:'Board Test #2', url: 'board2', mods:'Magistrates', summary:'This is also a test.'}];
  }

  triggerAccordian(): void {
    this.isClosed = !this.isClosed;
  }

  navigateToBoard(id: number): void {
    if(this.boardList.length > 0 && id <= this.boardList.length) {
      console.log(`${this.urlString}/${this.boardList[id].url}`);
      this.router.navigateByUrl(`/${this.urlString}/${this.boardList[id].url}`);
    }
  }
}
