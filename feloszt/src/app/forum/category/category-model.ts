export class CategoryModel {
    title: string;
    boardList: Array<any> = [];
    hierarchy: number;
}
