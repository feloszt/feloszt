export class ForumModel {
    categories: object[];
    

    constructor() {
        this.categories=[{name: 'The Portal', url:'portal'},
        {name: 'Abaetha', url:'abaetha'},
        {name: 'Megoldas', url:'megoldas'},
        {name: 'Dashatli', url:'dashatli'},
        {name: 'The Isles of Sythrio', url:'sythrio'},
        {name: 'Realm of Life (OOC)', url:'real-life'},
        {name: 'Tips and Tricks', url:'tips-tricks'}];
    }
}
