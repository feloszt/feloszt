import { Post } from '../post/post';

export class ThreadModel {
  title: string;
  author: string;
  createDateTime: Date;
  updatedDateTime;
  posts: Post[] = [];
  threadPartners: string[] = [];
  postCount: number;
  wordCount: number;
}
