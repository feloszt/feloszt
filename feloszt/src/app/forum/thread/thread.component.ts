import { Component, OnInit } from '@angular/core';
import { ThreadModel } from './thread-model';
import { Post } from '../post/post';

@Component({
  selector: 'app-thread',
  templateUrl: './thread.component.html',
  styleUrls: ['./thread.component.css']
})
export class ThreadComponent implements OnInit {

  threadModel: ThreadModel;

  constructor() { }

  ngOnInit() {
    this.threadModel = new ThreadModel();
    const p = new Post('Testing Thread', 'Tester mcTest', 'Test test test test test test test.');
    for ( let i = 0; i < 4 ; i++) {
      this.threadModel.posts.push(p.getPost());
      this.threadModel.threadPartners.push(p.author);
    }
    this.threadModel.postCount = this.threadModel.posts.length;
    this.threadModel.author = this.threadModel.posts[0].author;
    this.threadModel.title = this.threadModel.posts[0].title;
    //this.threadModel.dateTimeStamp = this.threadModel.posts[0].dateTimestamp;
  }

}
