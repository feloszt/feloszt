import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForumModel } from './forum-model';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [ForumModel],
  declarations: []
})
export class ForumModule { }
