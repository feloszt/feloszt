import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
  
  @Component({
    selector: 'app-board',
    templateUrl: './board.component.html',
    styleUrls: ['./board.component.css']
  })
   
export class BoardComponent implements OnInit {
  columns=[];
  dataSource=[];
  contentType='thread';
  ngOnInit(){
    this.columns=['status', 'title' , 'author','posts','views', 'updated', 'created'];
    this.dataSource=[
      {status:'open', title: 'Title 1', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 4', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 5', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 6', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 7', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 1', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 4', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 5', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 6', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 7', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 1', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 4', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 5', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 6', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 7', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 1', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 4', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 5', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 6', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 7', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 1', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 4', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 5', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 6', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 7', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 1', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 4', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 5', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 6', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 7', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 1', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 4', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 5', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 6', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 7', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 1', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 4', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 5', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 6', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 7', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 1', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 4', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 5', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 6', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 7', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},     
      {status:'open', title: 'Title 1', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 4', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 5', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 6', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'closed', title: 'Title 7', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'},
      {status:'open', title: 'Title 8', author: 'Glitch', posts: 3 , views: 10, updated: '02/02/2019', created: '02/02/2019'}];
  }
}
