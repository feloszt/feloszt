import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardComponent } from './board.component';
import { TableModule } from 'src/shared/table/table.module';

@NgModule({
  imports: [
    CommonModule,
    TableModule
  ],
  declarations: [BoardComponent]
})
export class BoardModule { }
