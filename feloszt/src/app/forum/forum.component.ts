import { Component, OnInit } from '@angular/core';
import { ForumModel } from './forum-model';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.css']
})
export class ForumComponent implements OnInit {
  forumModel: ForumModel;
  constructor() { }

  ngOnInit() {
    this.forumModel = new ForumModel();
  }

}
