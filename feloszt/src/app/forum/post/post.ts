export class Post {
  author: string;
  dateTimestamp: Date;
  wordCount: number;
  body: string;
  title: string;

  constructor(title: string, author: string, body: string) {
    this.title = title;
    this.dateTimestamp = new Date();
    this.author = author;
    this.body = body;
    this.wordCount = body.length;
  }

  getPost(): Post {
    return this;
  }
}
