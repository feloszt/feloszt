import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminForumsComponent } from './admin-forums.component';

describe('AdminForumsComponent', () => {
  let component: AdminForumsComponent;
  let fixture: ComponentFixture<AdminForumsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminForumsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminForumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
