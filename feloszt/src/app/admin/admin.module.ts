import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateRoleComponent } from '../feature/role/create-role/create-role.component';
import { AdminComponent } from './admin.component';
import { AdminRolesComponent } from './admin-roles/admin-roles.component';
import { AdminForumsComponent } from './admin-forums/admin-forums.component';
import { AdminMainComponent } from './admin-main/admin-main.component';
import { AdminMembersComponent } from './admin-members/admin-members.component';
import { MatDialogModule } from '@angular/material/dialog';
import { OverlayModule } from '@angular/cdk/overlay';
import { AdminNavBarComponent } from './admin-nav-bar/admin-nav-bar.component';
import { AdminNavBarModule } from './admin-nav-bar/modules/admin-nav-bar.module';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminRolesService } from './admin-roles/admin-roles.service';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    OverlayModule,
    AdminNavBarModule
  ],
  declarations: [
    AdminRolesComponent,
    AdminForumsComponent,
    AdminMainComponent,
    AdminMembersComponent,
    AdminNavBarComponent,
    AdminHomeComponent,
    AdminComponent
  ],
  entryComponents: [CreateRoleComponent],
  exports: [AdminRolesComponent, AdminForumsComponent, AdminMainComponent, AdminMembersComponent, AdminComponent],
  providers: [ AdminRolesService ]
})
export class AdminModule { }
