import { TestBed } from '@angular/core/testing';

import { AdminRolesService } from './admin-roles.service';

describe('AdminRolesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminRolesService = TestBed.get(AdminRolesService);
    expect(service).toBeTruthy();
  });
});
