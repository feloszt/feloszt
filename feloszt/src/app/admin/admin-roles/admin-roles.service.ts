import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { RoleModel, Role } from 'src/app/feature/role/role-model';
import { takeWhile, catchError } from 'rxjs/operators';
import { throwError, Observable ,Subject} from 'rxjs';

@Injectable()
export class AdminRolesService {

private httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
    })
};
  private isSubscribed = true;
  baseURL: string = environment.baseUrl;

  private rolesSource = new Subject<Role[]>();
  roles = this.rolesSource.asObservable();

  constructor(private http: HttpClient) {}

  killSubscriptions() {
    this.isSubscribed = false;
  }

  populateModel(): void {
    // const url = this.baseURL + '/roles';
    // this.http.get<Role[]>(url).pipe(takeWhile(() => this.isSubscribed)).subscribe(data => this.rolesSource.next(data));
  }

  addRole(role: Role): void {
    // const url = this.baseURL + '/roles';
    // this.http.post<Role>(url, role, this.httpOptions).subscribe(data => this.rolesSource.next([data]));
  }
}
