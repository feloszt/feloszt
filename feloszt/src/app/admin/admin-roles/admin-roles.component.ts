import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RoleModel, Role } from 'src/app/feature/role/role-model';
import { CreateRoleComponent } from 'src/app/feature/role/create-role/create-role.component';
import { AdminRolesService } from './admin-roles.service';

@Component({
  selector: 'app-admin-roles',
  templateUrl: './admin-roles.component.html',
  styleUrls: ['./admin-roles.component.css']
})
export class AdminRolesComponent implements OnInit {
  roles: Role[];
  displayedColumns: string[] = ['Role Name', 'Description', 'Type'];

  constructor(public dialog: MatDialog, private ref: ChangeDetectorRef, private adminRolesService: AdminRolesService) {
    //this.adminRolesService.populateModel();
  }

  ngOnInit() {
    this.adminRolesService.roles.subscribe(model => this.roles = model);
    this.ref.detectChanges();
  }

  createRole(): void {
   const dialogRef = this.dialog.open(CreateRoleComponent, { });
    dialogRef.afterClosed().subscribe((result: Role) => {
      if (result !== null) {
        this.adminRolesService.addRole(result);
      }
    });
    this.ref.detectChanges();
  }
}
