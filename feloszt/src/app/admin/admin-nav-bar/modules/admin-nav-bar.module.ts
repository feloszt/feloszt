import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminRolesComponent } from '../../admin-roles/admin-roles.component';
import { AdminForumsComponent } from '../../admin-forums/admin-forums.component';
import { AdminMembersComponent } from '../../admin-members/admin-members.component';
import { AdminMainComponent } from '../../admin-main/admin-main.component';

const adminRoutes = [
  {
    path: 'roles',
    component: AdminRolesComponent,
    outlet: 'admin-router'
  },
  {
    path: 'forums',
    component: AdminForumsComponent,
    outlet: 'admin-router'
  },
  {
    path: 'members',
    component: AdminMembersComponent,
    outlet: 'admin-router'
  },
  {
    path: 'site',
    component: AdminMainComponent,
    outlet: 'admin-router'
  }
];

@NgModule({
imports: [RouterModule.forRoot(adminRoutes)],
exports: [RouterModule],
providers: []
})

export class AdminNavBarModule { }
