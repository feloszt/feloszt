import { AdminNavBarModule } from './admin-nav-bar.module';

describe('AdminNavBarModule', () => {
  let adminNavBarModule: AdminNavBarModule;

  beforeEach(() => {
    adminNavBarModule = new AdminNavBarModule();
  });

  it('should create an instance', () => {
    expect(adminNavBarModule).toBeTruthy();
  });
});
