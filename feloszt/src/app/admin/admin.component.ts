import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RoleModel } from '../feature/role/role-model';
import { AdminRolesService } from './admin-roles/admin-roles.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminComponent implements OnDestroy, OnInit {
  constructor(public dialog: MatDialog, private ref: ChangeDetectorRef, private adminRoleServices: AdminRolesService,
              private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {}

  triggerModel(): void {
    this.adminRoleServices.populateModel();
  }

  gotoPage(page: string): void {
    this.router.navigateByUrl(`admin/${page }`);
  }

  ngOnDestroy(): void {
    this.adminRoleServices.killSubscriptions();
  }

}
