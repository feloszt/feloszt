import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { LoginRegisterComponent } from './login-register.component';
import { LoginRegisterService } from './login-register.service';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ LoginRegisterComponent ],
  entryComponents: [ LoginRegisterComponent ],
  providers: [LoginRegisterService]
})
export class LoginRegisterModule { }

