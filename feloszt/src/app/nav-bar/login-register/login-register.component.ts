import { Component, OnInit, ChangeDetectorRef, EventEmitter, Output } from '@angular/core';
import { LoginRegisterModel } from './login-register-model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginRegisterService } from './login-register.service';
import { takeWhile } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { PlayerModel } from 'src/app/player/player-model';

@Component({
  selector: 'app-login-register',
  templateUrl: './login-register.component.html',
  styleUrls: ['./login-register.component.css']
})
export class LoginRegisterComponent implements OnInit {

  @Output() validLogin: EventEmitter<string> = new EventEmitter<string>();
  constructor(private ref: ChangeDetectorRef, private service: LoginRegisterService, private router: Router,
              private rootService: AppService) { }

  loginRegisterModel: LoginRegisterModel;
  player: PlayerModel;
  form: FormGroup;
  formBuilder: FormBuilder = new FormBuilder();
  subscribeFlag: boolean = true;

  ngOnInit(): void {
    this.loginRegisterModel = new LoginRegisterModel();
    this.loginRegisterModel.signup = true;
    this.loginRegisterModel.buttonText = 'Create Account';
    this.loginRegisterModel.title = 'Register Account';

    this.form = this.generateForm();
  }

  ngOnDestroy(): void {
    this.subscribeFlag = false;
  }

  haveAccount(): void {
    this.loginRegisterModel.signup = false;
    this.loginRegisterModel.buttonText = 'Login';
    this.loginRegisterModel.title = 'Sign In';
  }

  login(): void {
    const errors: Array<object> = [];
    this.validateForm();
    this.ref.detectChanges();

    if (this.form.valid) {
      if (this.loginRegisterModel.signup) {
        const pword = this.form['controls']['password'].value;
        const pword2 = this.form['controls']['passwordConfirm'].value;
            if (pword !== pword2) {
              errors.push({'control': 'passwordConfirm', 'error': 'Passwords do not match'});
              this.loginRegisterModel.addErrors(errors);
            } else {
              this.service.registerUser(this.form['controls']['accountName'].value,pword, this.form['controls']['email'].value)
              .pipe(takeWhile(() => this.subscribeFlag)).subscribe(data => {
                  if (data['success']) {
                    const obj: object = { data: data, user: this.form['controls']['accountName'].value};
                    this.validationApproved(obj);
                  }
                });
            }
      } else {
        this.service.validateLogin(this.form['controls']['accountName'].value,this.form['controls']['password'].value)
          .pipe(takeWhile(() => this.subscribeFlag)).subscribe(data => {
            if (data['success']) {
              const obj: object = { data: data, user: this.form['controls']['accountName'].value};
              this.validationApproved(obj);
            }
          });
      }
    }
  }

  private generateForm(): FormGroup {
    return this.formBuilder.group({
      accountName: ['', [Validators.minLength(3), Validators.maxLength(50), Validators.required]],
      password: ['', [Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]],
      passwordConfirm: ['', [Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]],
      email: [''],
      emailConfirm: [''],
      ageValidation: [false],
      tosAgreement: [false],
      creativeAgreement: [false]
    });
  }

  private validateForm(): void {
    this.loginRegisterModel.errorMessages = [];
    const errors: Array<object> = [];
    Object.keys(this.form.controls).forEach(field => {
      const control = this.form.get(field);
      control.updateValueAndValidity();
      if(control['error'] !== null) {
      errors.push({'control': `${field}`, 'error' : control['errors'] });
      }
    });
    if(errors !== []) {
      this.loginRegisterModel.addErrors(errors);
      this.form.updateValueAndValidity();
    }
  }

  private validationApproved(event: any): void {
    if(event['data']['success']) {     
      this.service.saveData(event['data']);
      this.rootService.fetchUserRoles(event['data']['user']).subscribe((data: any) => {
        console.log(data);
        if (data && data['success']) {
          this.rootService.setUser(data);
        }
      });
      this.rootService.adminRoleCheck();
      // this.router.navigateByUrl('/landing');
    }
  }
}
