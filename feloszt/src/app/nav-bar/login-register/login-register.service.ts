import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AppService } from 'src/app/app.service';

@Injectable({
  providedIn: 'root'
})
export class LoginRegisterService {

  constructor(private http: HttpClient) { }

  registerUser(name: string, password: string, email: string): Observable<any> {
    const payload: object = {
      displayName: name,
      password: password,
      emailAddress: email
    };
    return this.http.post<any>(`${environment.baseAPIURL}/members/register`, payload);
  }
  validateLogin(name: string, password: string): Observable<object> {
    const payload = {displayName: name, password: password};
    return this.http.post<object>(`${environment.baseAPIURL}/members/login`, payload);
  }

  saveData(data: object): void {
    localStorage.setItem('token', data['token']);
    localStorage.setItem('user', data['user']);
  }
}
