export class LoginRegisterModel {
  signup: boolean;
  title: string;
  buttonText: string;
  errorMessages: string[] = [];

  addErrors(errors: Array<object>): void {
    errors.forEach((error: object) => {
      if (error['error'] !== null) {
        switch (error['control']) {
          case 'accountName':
          if (error['error']['minLength']) {
            this.errorMessages.push('Account Name must be at least 3 characters long.');
          } else if (error['error']['maxLength']) {
            this.errorMessages.push('Account Name cannot exceed 50 characters long.');
          }
          console.log(error['error']);
          //   this.errorMessages.push('Account Name is Required.');
            break; //['', [Validators.minLength(3), Validators.maxLength(50), Validators.required]],
          case 'password':
            if (error['error']['pattern']) {
              this.errorMessages.push(
                `Password must have at least 8 alphanumeric, 1 special character, and at least 1 capital letter.
                  Special Characters are: !@#$%^&`);
            }
            break; //['', [Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]],
          case 'passwordConfirm':
          if (error['error']['pattern']) {
            this.errorMessages.push(
              `Password must have at least 8 alphanumeric, 1 special character, and at least 1 capital letter.
                Special Characters are: !@#$%^&`);
          }
          this.errorMessages.push('Passwords do not match');
            break;//[''],
          case 'email': break;//[''],
          case 'emailConfirm': break;//[''],
          case 'ageValidation': break;//[false],
          case 'tosAgreement': break;//[false],
          case 'creativeAgreement': break;//[false]
          default: break;
        }
      }
    });
  }
}
