import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './nav-bar.component';
import { LoginRegisterModule } from './login-register/login-register.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginRegisterComponent } from './login-register/login-register.component';
import { HttpClientModule } from '@angular/common/http';
import { NavBarService } from './nav-bar.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LoginRegisterModule,
    HttpClientModule
    ],
  providers: [NavBarService],
  declarations: [NavBarComponent, LoginRegisterComponent]
})
export class NavBarModule { }
