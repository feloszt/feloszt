import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { NavBar } from './nav-bar';

@Injectable({
  providedIn: 'root'
})
export class NavBarService {

  constructor(private http: HttpClient) { 
    // this.model = new NavBar();
  }

  checkIfPlayerIsLoggedIn(): boolean {
    // this.model.playerLoggedIn = (localStorage.getItem('token')) ? true : false;
    this.validateToken(localStorage.getItem('token')).subscribe(data => this.setPlayer(data));
//      this.player = localStorage.getItem('token');
    // return this.model.playerLoggedIn;
    return (localStorage.getItem('token')) ? true : false;
  }

  // getModel(): NavBarModel {
  //   return this.model;
  // }

  private validateToken(token: string): Observable<object> {
    const payload: object = {token: token};
    return this.http.post<object>('/felo/auth/validate/', payload);
  }

  private setPlayer(data: any): void{
    console.log(data);
  }
}
