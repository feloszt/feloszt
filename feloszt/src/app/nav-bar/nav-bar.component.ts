import { Component, OnInit, ElementRef, Input, ChangeDetectionStrategy, AfterViewInit, ChangeDetectorRef, OnChanges } from '@angular/core';
import { LoginRegisterService } from './login-register/login-register.service';
import { NavBarService } from './nav-bar.service';
import { PlayerModel } from '../player/player-model';
import { AppService } from '../app.service';
// import { NavBarModel } from './nav-bar';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class NavBarComponent implements AfterViewInit, OnChanges, OnInit {
  elementRef: ElementRef;
  // model: NavBarModel;
  @Input() imageSource: string;
  @Input() player: PlayerModel;
  @Input('changed') set changed (value: boolean) {
    if (value) {
      this.ref.detectChanges();
    }
  }

  private isAdmin: boolean = false;
  constructor(private ref: ChangeDetectorRef,  private service: NavBarService, private rootService: AppService) {
    // this.model = new NavBarModel();
  }

  ngOnInit() {
    // this.model = this.service.getModel();

  }
  ngAfterViewInit(): void {
    this.isAdmin = this.rootService.getAdminRoleCheck();
    console.log(this.service.checkIfPlayerIsLoggedIn());
    console.log(`is the logged in player a mod: ${this.isAdmin}`);
  }
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    console.log(this.service.checkIfPlayerIsLoggedIn());
  }
  logout(): void {
    // this.model.playerLoggedIn=false;
    localStorage.removeItem('token');
    this.isAdmin = false;
    this.ref.detectChanges();
  }
  playerLoggedIn(): boolean {
    return (this.player !== null);
  }
  getIsAdmin(): boolean {
    this.isAdmin = this.rootService.getAdminRoleCheck();
    console.log(`is the logged in player a mod: ${this.isAdmin}`);
    return this.isAdmin;
  }
}
