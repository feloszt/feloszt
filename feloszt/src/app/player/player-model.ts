import { Character } from './character/character';
import { Settings } from './player-settings/settings';

// this is the user not the character
export class PlayerModel {
  userName: string;
  characters: Array<Character>;
  personalSettings: Settings;
  userRoles: string[];
  //
}
