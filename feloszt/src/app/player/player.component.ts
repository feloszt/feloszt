import { Component, OnInit } from '@angular/core';
import { PlayerModel } from './player-model';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

  playerModel: PlayerModel;
  constructor() { }

  ngOnInit() {
    this.playerModel = new PlayerModel();
    this.playerModel.userName = 'Glitch';
  }

}
