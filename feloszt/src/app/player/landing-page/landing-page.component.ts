import { Component, OnInit } from '@angular/core';
import { PlayerModel } from '../player-model';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  player: PlayerModel = new PlayerModel();
  constructor(private rootService: AppService) { }

  ngOnInit() {
    if (localStorage.getItem('player')) {
      this.player = JSON.parse(localStorage.getItem('player'));
      console.log(this.player);
    }
  }

}
