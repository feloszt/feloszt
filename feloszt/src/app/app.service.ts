import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PlayerModel } from './player/player-model';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private player: PlayerModel = new PlayerModel();
  private isAdmin: boolean = false;

  constructor(private http: HttpClient) { }
  fetchUserRoles(name: string): Observable<any> {
    this.player.userName = name;
 
    return this.http.get<any>(`${environment.baseAPIURL}/roles/memberroles/${name}`);
  }

  setUser(data: any): void {
    this.player.userRoles = data['roles'];
    console.log(this.player);
    localStorage.setItem('player', JSON.stringify(this.player));

  }

  getUser(): PlayerModel {
    return this.player;
  }

  adminRoleCheck(): void {
    console.log(this.player);
    if (this.player !== null && this.player['userRoles']) {
      this.player['userRoles'].forEach((role: string) => {
        console.log(role);
        switch(role) {
          case 'Celestial':
            this.isAdmin = true;
            break;
          default: 
            this.isAdmin = false;
            break;
        }
      });
    }
    console.log(this.isAdmin);
  }

  getAdminRoleCheck(): boolean {
    return this.isAdmin;
  }
}
