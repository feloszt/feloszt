import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AdminModule } from './admin/admin.module';
import { AppComponent } from './app.component';
import { ForumComponent } from './forum/forum.component';
import { ProfileComponent } from './profile/profile.component';
import { InboxComponent } from './inbox/inbox.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { CategoryComponent } from './forum/category/category.component';
import { ThreadComponent } from './forum/thread/thread.component';
import { PostComponent } from './forum/post/post.component';
import { BoardComponent } from './forum/board/board.component';
import { RoleComponent } from './feature/role/role.component';
import { PlayerComponent } from './player/player.component';
import { CreateRoleModule } from './feature/role/create-role/create-role.module';
import { CharacterComponent } from './player/character/character.component';
import { PlayerSettingsComponent } from './player/player-settings/player-settings.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OverlayModule } from '@angular/cdk/overlay';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from 'src/app/admin/admin.component';
import { AdminRolesComponent } from 'src/app/admin/admin-roles/admin-roles.component';
import { MatTabHeader, MatFormFieldModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { AdminAccessGuard } from './guards/admin-access.guard';
import {LoginRegisterComponent} from './nav-bar/login-register/login-register.component';
import { LoginRegisterModule } from './nav-bar/login-register/login-register.module';
import { TableModule } from 'src/shared/table/table.module';
import { LoginGuard } from './guards/login.guard';
import { LandingPageComponent } from './player/landing-page/landing-page.component';
import { LandingPageModule } from './player/landing-page/landing-page.module';
import { AdminMembersComponent } from './admin/admin-members/admin-members.component';
import { AdminForumsComponent } from './admin/admin-forums/admin-forums.component';
import { AdminMainComponent } from './admin/admin-main/admin-main.component';
import { PostEditorModule } from 'src/shared/post-editor/post-editor.module';
import { ContentEditableDirective } from '../shared/directives/content-editable.directive';

const routes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AdminAccessGuard, LoginGuard],
    children: [
      {
        path: 'roles',
        canActivate: [AdminAccessGuard],
        component: AdminRolesComponent
      },
      {
        path: 'members',
        canActivate: [AdminAccessGuard],
        component: AdminMembersComponent
      },
      {
        path: 'forums',
        canActivate: [AdminAccessGuard],
        component: AdminForumsComponent
      },
      {
        path: 'site',
        canActivate: [AdminAccessGuard],
        component: AdminMainComponent
      }
    ]
  },
  {
    path: 'index',
    component: ForumComponent
  },
  {
    path: 'inbox',
    component: InboxComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: ':category/:board',
    component: BoardComponent
  },
  {
    path: ':category/:board/:threadID',
    component: ThreadComponent
  },
  {
    path: 'login',
    component: LoginRegisterComponent
  },
  {
    path: 'landing',
    component: LandingPageComponent
  },
  { path: '',
    redirectTo: 'index',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'index'
  }
];

@NgModule({
  declarations: [
    MatTabHeader,
    AppComponent,
    ForumComponent,
    ProfileComponent,
    InboxComponent,
    NavBarComponent,
    CategoryComponent,
    ThreadComponent,
    PostComponent,
    BoardComponent,
    RoleComponent,
    PlayerComponent,
    CharacterComponent,
    PlayerSettingsComponent,
    // ContentEditableDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    MatDialogModule,
    BrowserAnimationsModule,
    CreateRoleModule,
    OverlayModule,
    RouterModule.forRoot(routes),
    AdminModule,
    LoginRegisterModule,
    MatFormFieldModule,
    TableModule,
    LandingPageModule,
    PostEditorModule
  ],
  exports: [RouterModule],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
