import { Component, OnInit, HostListener, ViewChild, AfterViewInit } from '@angular/core';
import { PlayerModel } from './player/player-model';
import { Router } from '@angular/router';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit, OnInit {
  title = 'Feloszt';
  player: PlayerModel = new PlayerModel();
  navChange: boolean = false;
  @ViewChild(NavBarComponent)
  @ViewChild('navbar') navbar: NavBarComponent;
  constructor(private router: Router, private rootService: AppService) {}

  ngOnInit(): void {
    const token: string = (localStorage.getItem('token')) ? localStorage.getItem('token') : null;
    const userName: string = (localStorage.getItem('user')) ? localStorage.getItem('user') : null;
    this.setup(token,userName);
  }

  ngAfterViewInit(): void {
    this.navChange = true;
  }

  gotoPage(page: string): void {
    this.router.navigate([`${page}`]);
  }

  @HostListener('window:resize')
  onResize(): string {
    this.navChange = true;
    return (window.innerWidth >= 800 ) ? '../../assets/felo-banner.png' : '../../assets/felo-mobile-banner.png';
  }

  private setup(token: string, userName: string): void {
    console.log(userName);
    if (token !== null && userName !== null) {
      this.rootService.fetchUserRoles(userName).subscribe((data: any) => {
        if (data && data['success']) {
          this.rootService.setUser(data);
          this.player = this.rootService.getUser();
          this.rootService.adminRoleCheck();
        }
      });
      this.router.navigateByUrl('/landing');
    }
  }

}
