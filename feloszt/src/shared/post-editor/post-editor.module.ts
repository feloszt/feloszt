import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostEditorComponent } from './post-editor.component';
import { FormsModule } from '@angular/forms';
import { ContentEditableDirective } from '../directives/content-editable.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [PostEditorComponent],
  declarations: [PostEditorComponent, ContentEditableDirective]
})

export class PostEditorModule { }
