import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnChanges } from '@angular/core';
import { PostEditorModel } from './post-editor-model';

@Component({
  selector: 'app-post-editor',
  templateUrl: './post-editor.component.html',
  styleUrls: ['./post-editor.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostEditorComponent implements OnInit {
  model: PostEditorModel = new PostEditorModel();
  postBodyElement: any;

  constructor(readonly ref: ChangeDetectorRef) { }

  ngOnInit() {
  }
  
  onClickButtonCommand(event: any): void {
    this.postBodyElement = document.getElementById('post-body');
    document.execCommand(event['command'], false, '');
    this.model.postBody = this.postBodyElement.innerHTML;
    this.ref.detectChanges();
  }

}
