
export class PostEditorModel {
    controls: Array<object> = [];
    postBody: string = '';
    allowHTML: boolean = true;
    editable: boolean = true;
    
    constructor() {
        this.controls = [
            {
                command: 'bold',
                type: 'button',
                innerHTML: 'b'
            },
            {
                command: 'italic',
                type: 'button',
                innerHTML: 'i'
            },
            {
                command: 'underline',
                type: 'button',
                innerHTML: 'u'
            }
        ]
    }
}
