import { MatTableDataSource } from "@angular/material";

export class TableModel {
    displayColumns: string[];
    dataSource: MatTableDataSource<any>;
    pageIndex: number;
    pageSize: number;
    lowValue: number;
    highValue: number;  
    contentType: string;
}
