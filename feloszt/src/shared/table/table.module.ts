import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table.component';
import {  MatSort, MatHeaderRow, MatHeaderCell, MatHeaderCellDef,
  MatHeaderRowDef, MatSortHeader, MatRow, MatRowDef, MatCell, MatCellDef, MatFormFieldModule, MatTableModule,
  MatPaginatorModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatTableModule
    ],
  declarations: [
    TableComponent,
    MatSortHeader,
    MatSort
  ],
  exports: [ TableComponent ]
})
export class TableModule { }
