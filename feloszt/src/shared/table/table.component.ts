import { Component, ViewChild, Input } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})

export class TableComponent {

  @Input() set columns(value: string[]) {
    if(value) {
      this.displayColumns = value;
    }
  }

  @Input() set data(source: any){
    if(source) {
      this.dataSource = source;
    }
  }
  @Input() set contentType(type: string){
    this.contentTypeIndicator = type;
  }

  displayColumns: string[]=[];
  dataSource: MatTableDataSource<any>;
  pageIndex:number = 0;
  pageSize:number = 10;
  lowValue:number = 0;
  highValue:number= 10;  

  private contentTypeIndicator:string;
 
    @ViewChild('MatPaginator') paginator: MatPaginator;
    @ViewChild('MatSort') sort: MatSort;
  
    ngAfterViewInit() {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  
    applyFilter(filterValue: string) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    selectedRow(event: any): void{
      console.log(event);

      /*
      author: "Glitch"
created: "02/02/2019"
posts: 3
status: "open"
title: "Title 5"
updated: "02/02/2019"
views: 10
      */
      switch(this.contentTypeIndicator) {
        case 'thread':
          //trigger a thread search, find, display.
          break;
        default: break;
      }
    }     

  getPaginatorData(event: any) {
     if(event.pageIndex == this.pageIndex + 1){
        this.lowValue = this.lowValue + this.pageSize;
        this.highValue =  this.highValue + this.pageSize;
       }
    else if(event.pageIndex == this.pageIndex - 1){
       this.lowValue = this.lowValue - this.pageSize;
       this.highValue =  this.highValue - this.pageSize;
      }   
       this.pageIndex = event.pageIndex ;
 }
  }
